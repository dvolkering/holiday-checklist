<?php
// Application middleware

use HolidayChecklist\Middleware\RequireHTTPS;
use Respect\Validation\Validator as v;

$app->add(new RequireHTTPS());

$app->add(new \HolidayChecklist\Middleware\Auth\ValidationErrorsMiddleware($container));
$app->add(new \HolidayChecklist\Middleware\Auth\OldInputMiddleware($container));
$app->add(new \HolidayChecklist\Middleware\Auth\CsrfViewMiddleware($container));

$app->add($container->csrf);

v::with('HolidayChecklist\\Validation\\Rules\\');