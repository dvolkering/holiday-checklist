<?php

use HolidayChecklist\Controller\AuthController;
use HolidayChecklist\Controller\CategoryController;
use HolidayChecklist\Controller\ChecklistController;
use HolidayChecklist\Controller\ItemController;
use HolidayChecklist\Controller\PasswordController;
use HolidayChecklist\Middleware\Auth\AuthMiddleware;
use HolidayChecklist\Middleware\Auth\GuestMiddleware;

// Homepage
$app->get('/', ChecklistController::class . ':list')->setName('home')->add(new AuthMiddleware($container));

/**
 * Checklists
 */
$app->group('/checklist', function () use ($app) {
    $app->get('/view/[{id}]', ChecklistController::class . ':view');
    $app->get('/add/[{id}]', ChecklistController::class . ':add')->setName('checklist.add');
    $app->post('/add/[{id}]', ChecklistController::class . ':save');
    $app->get('/remove/[{id}]', ChecklistController::class . ':remove');
    $app->get('/duplicate/[{id}]', ChecklistController::class . ':duplicate');
})->add(new AuthMiddleware($container));

/**
 * Categories
 */
$app->group('/category', function () use ($app) {
    $app->post('/save', CategoryController::class . ':save');
    $app->post('/delete', CategoryController::class . ':delete');
})->add(new AuthMiddleware($container));

/**
 * Items
 */
$app->group('/item', function () use ($app) {
    $app->post('/save', ItemController::class . ':save');
    $app->post('/save-order', ItemController::class . ':saveOrder');
})->add(new AuthMiddleware($container));

/**
 * Authentication
 */
$app->group('/auth', function () {
    $this->get('/signup', AuthController::class . ':getSignUp')->setName('auth.signup');
    $this->post('/signup', AuthController::class . ':postSignUp');
    $this->get('/signin', AuthController::class . ':getSignIn')->setName('auth.signin');
    $this->post('/signin', AuthController::class . ':postSignIn');
})->add(new GuestMiddleware($container));

$app->group('/auth', function () {
    $this->get('/signout', AuthController::class . ':getSignOut')->setName('auth.signout');
    $this->get('/password/change', PasswordController::class . ':getChangePassword')->setName('auth.password.change');
    $this->post('/password/change', PasswordController::class . ':postChangePassword');
})->add(new AuthMiddleware($container));

