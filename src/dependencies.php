<?php
// DIC configuration

use HolidayChecklist\Auth\Auth;
use HolidayChecklist\Validation\Validator;

$container = $app->getContainer();

// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
    return $logger;
};

// Service factory for the ORM
$capsule = new \Illuminate\Database\Capsule\Manager;
$capsule->addConnection($container['settings']['db']);
$capsule->setAsGlobal();
$capsule->bootEloquent();

$container['db'] = function ($container) use ($capsule) {
    return $capsule;
};

// Register component on container
$container['view'] = function ($container) {
    $view = new \Slim\Views\Twig('../templates', [
//        'cache' => 'cache'
        'cache' => false,
        'debug' => true
    ]);

    $view->getEnvironment()->addGlobal('auth', [
        'check' => $container->auth->check(),
        'user' => $container->auth->user()
    ]);

    // Instantiate and add Slim specific extension
    $basePath = rtrim(str_ireplace('index.php', '', $container['request']->getUri()->getBasePath()), '/');
    $view->addExtension(new Slim\Views\TwigExtension($container['router'], $basePath));
    $view->addExtension(new Twig_Extension_Debug());
//    $view->addExtension(new Twig_Extensions_Extension_Intl());

    return $view;
};

$container['auth'] = function ($container) {
    return new Auth;
};

$container['validator'] = function ($container) {
    return new Validator;
};

$container['AuthController'] = function ($container) {
    return new \HolidayChecklist\Controller\AuthController($container);
};

$container['PasswordController'] = function ($container) {
    return new \HolidayChecklist\Controller\PasswordController($container);
};

$container['csrf'] = function ($container) {
    $test = null;
    return new \Slim\Csrf\Guard('csrf', $test, null, 200, 16, true);
};