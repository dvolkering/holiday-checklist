<?php

namespace HolidayChecklist\Middleware;

class RequireHTTPS {
    public function __invoke($request, $response, $next) {
        if ($request->getUri()->getScheme() !== 'https' && !str_contains($request->getUri()->getHost(), 'localhost')) {
            $https_url = 'https://' . $request->getUri()->getHost() . $request->getUri()->getPath();

            return $response->withRedirect($https_url, 301);
        } else {
            $response = $next($request, $response);

            return $response;
        }
    }
}
