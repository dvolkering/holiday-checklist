<?php

namespace HolidayChecklist\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Category
 *
 * @property int $id
 * @property int $checklist_id
 * @property string $name
 *
 * @package HolidayChecklist\Model
 * @author Dennis Volkering
 */
class Category extends Model {
    protected $table = 'categories';
    protected $guarded = ['id'];

    public function checklist() {
        return $this->belongsTo('HolidayChecklist\Model\Checklist');
    }

    public function items() {
        return $this->hasMany('HolidayChecklist\Model\Item');
    }
}
