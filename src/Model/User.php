<?php

namespace HolidayChecklist\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * User
 *
 * @author    Haven Shen <havenshen@gmail.com>
 * @copyright    Copyright (c) Haven Shen
 */
class User extends Model {
    public $first_name;
    public $last_name;
    public $email;
    protected $table = 'users';
    protected $fillable = [
        'email',
        'name',
        'password',
    ];

    public function setPassword($password) {
        $this->update([
            'password' => password_hash($password, PASSWORD_DEFAULT)
        ]);
    }

    public function getFirstName() {
        return $this->first_name;
    }

    public function setFirstName($firstName) {
        $this->first_name = trim($firstName);
    }

    public function getLastName() {
        return $this->last_name;
    }

    public function setLastName($lastName) {
        $this->last_name = trim($lastName);
    }

    public function getEmailVariables() {
        return [
            'full_name' => $this->getFullName(),
            'email' => $this->getEmail(),
        ];
    }

    public function getFullName() {
        return "$this->first_name $this->last_name";
    }

    public function getEmail() {
        return $this->email;
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function checklists() {
        return $this->hasMany('HolidayChecklist\Model\Checklist');
    }

    public function subscribedChecklists() {
        return $this->belongsToMany('HolidayChecklist\Model\Checklist', 'users_checklists');
    }
}