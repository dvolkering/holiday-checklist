<?php

namespace HolidayChecklist\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Checklist
 *
 * @property int $id
 * @property string $name
 * @property string $date_start
 * @property string $date_end
 * @property int $public_view
 * @property int $public_edit
 * @property int $user_id
 *
 * @package HolidayChecklist\Model
 * @author Dennis Volkering
 */
class Checklist extends Model {
    protected $table = 'checklists';
    protected $guarded = ['id'];

    public function categories() {
        return $this->hasMany('HolidayChecklist\Model\Category');
    }

    public function admin() {
        return $this->belongsTo('HolidayChecklist\Model\User');
    }

    public function subscribers() {
        return $this->belongsToMany('HolidayChecklist\Model\User', 'users_checklists');
    }
}
