<?php

namespace HolidayChecklist\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Item
 *
 * @property int $id
 * @property int category_id
 * @property string $name
 * @property int $checked
 * @property int $order
 *
 * @package HolidayChecklist\Model
 * @author Dennis Volkering
 */
class Item extends Model {
    protected $table = 'items';
    protected $guarded = ['id'];

    public function category() {
        return $this->belongsTo('HolidayChecklist\Model\Category');
    }
}
