<?php

namespace HolidayChecklist\Validation\Rules;

use Respect\Validation\Rules\AbstractRule;

/**
 * MatchesPassword
 *
 * @author    Haven Shen <havenshen@gmail.com>
 * @copyright    Copyright (c) Haven Shen
 */
class MatchesPassword extends AbstractRule {
    protected $password;

    public function __construct($password) {
        $this->password = $password;
    }

    public function validate($input) {
        return password_verify($input, $this->password);
    }
}