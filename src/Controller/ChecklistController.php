<?php

namespace HolidayChecklist\Controller;

use HolidayChecklist\Model\Category;
use HolidayChecklist\Model\Checklist;
use HolidayChecklist\Model\Item;
use HolidayChecklist\Model\User;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use Slim\Container;
use Slim\Http\Request;
use Slim\Http\Response;

class ChecklistController {
    protected $container;
    protected $view;
    protected $router;
    /* @var DB */
    protected $db;

    public function __construct(Container $container) {
        $this->container = $container;
        $this->router = $container['router'];
        $this->view = $container['view'];
        $this->db = $container['db'];
    }

    /**
     * List all checklists
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     */
    public function list($request, $response, $args) {
        $args['checklists'] = Checklist::where(array('user_id' => $this->container->auth->user()->id, 'public_view' => 0))->orderBy('name')->get()->toArray();
        $args['public_checklists'] = Checklist::where(array('public_view' => 1))->orderBy('name')->get()->toArray();
        $args['subscibed_checklists'] = User::find($this->container->auth->user()->id)->subscribedChecklists()->get()->toArray();
        $args['baseurl'] = $request->getUri()->getBaseUrl();

        return $this->view->render($response, 'checklist-list.twig', $args);
    }

    /**
     * Add Checklist
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     */
    public function add($request, $response, $args) {
        $args['baseurl'] = $request->getUri()->getBaseUrl();
        $args['users'] = User::with('subscribedChecklists')->get()->all();

        if (isset($args['id'])) {
            $args['checklist'] = Checklist::find($args['id']);

            if (isset($args['checklist'])) {
                foreach (Checklist::find($args['id'])->subscribers()->get()->toArray() as $usr_selected) {
                    $args['users_selected'][] = $usr_selected['id'];
                }
            }
        } else {
            $args['checklist'] = new Checklist();
        }


        if (isset($args['id']) && $this->container->auth->user()->id != $args['checklist']->user_id) {
            return 'You shall not pass';
        }

        return $this->view->render($response, 'checklist-add.twig', $args);
    }

    /**
     * View Checklist
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     */
    public function view($request, $response, $args) {
        $args['baseurl'] = $request->getUri()->getBaseUrl();
        $args['checklist'] = Checklist::find($args['id']);
        $args['categories'] = Category::where(array('checklist_id' => $args['id']))->with(['items' => function ($query) {
            $query->orderBy('order');
        }])->get();

        if (AuthController::isUserAllowed($args['checklist'], $this->container->auth->user()->id)) {
            return $this->view->render($response, 'checklist-view.twig', $args);
        }

        return 'You shall not pass';
    }

    /**
     * Save Checklist
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     */
    public function save($request, $response, $args) {
        $data = $request->getParsedBody();
        $new = false;

        if ($data['id'] !== '' && isset($data['id'])) {
            $checklist = Checklist::find($data['id']);

            if ($checklist === null) {
                // Checklist is not found, create a new one
                $checklist = new Checklist();
                $new = true;
            } else {
                // Checklist is found, now check if it is from the current user.
                if ($this->container->auth->user()->id != $checklist->user_id) {
                    return 'You shall not pass';
                }
            }
        } else {
            // ID is not provided, create a new one
            $checklist = new Checklist();
            $new = true;
        }

        // Set all the data
        $checklist->id = $data['id'];
        $checklist->user_id = $this->container->auth->user()->id;
        $checklist->name = $data['name'];
        $checklist->public_view = $data['public_view'];
        $checklist->public_edit = $data['public_edit'];
        $checklist->date_start = $data['date_start'] !== '' ? $data['date_start'] : null;
        $checklist->date_end = $data['date_end'] !== '' ? $data['date_end'] : null;

        $selectedUsers = array();
        foreach ($data['user'] as $usrId => $selected) {
            if ($selected) {
                $selectedUsers[] = $usrId;
            }
        }

        // Unset the ID when it's a new entity
        if ($new) {
            unset($checklist->id);
        }

        try {
            if ($checklist->save()) {

                try {
                    $checklist->subscribers()->sync($selectedUsers);
                } catch (QueryException $exception) {
                    //todo fix error handling
                    return 'Cannot create a coupling between the checklist and an user';
                }

                return $response->withRedirect('/');
            } else {
                return $response->withRedirect('/checklist/add/' . $checklist->id);
            }
        } catch (QueryException $exception) {
            $args['baseurl'] = $request->getUri()->getBaseUrl();
            $args['checklist'] = $checklist;
            $args['error'] = $exception->getMessage();

            return $this->view->render($response, 'checklist-add.twig', $args);
        }

    }

    /**
     * TODO
     * Remove Checklist
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     */
    public function remove($request, $response, $args) {
        $checklistId = $args['id'];

        try {
            $this->db->getConnection()->beginTransaction();

            if ($checklistId !== '' && isset($checklistId)) {
                $checklist = Checklist::find($checklistId);
                $categories = Category::where(array('checklist_id' => $checklistId))->get();


                if ($this->container->auth->user()->id != $checklist->user_id) {
                    return 'You shall not pass';
                }

                foreach ($categories as $category) {
                    $items = Item::where(array('category_id' => $category->id))->get();

                    // First remove the items
                    foreach ($items as $item) {
                        $item->delete();
                    }

                    // Then remove the category
                    $category->delete();
                }

                // At last, we remove the checklist itself
                $checklist->delete();
            }
            $this->db->getConnection()->commit();

            return $response->withRedirect($this->router->pathFor('home'));
        } catch (\PDOException $e) {
            $this->db->getConnection()->rollback();

            return $response->withRedirect($this->router->pathFor('home'));
        }
    }

    /**
     * TODO:
     *  - Add frontend confirmation
     *  - Add checks
     *
     * Duplicate Checklist
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     */
    public function duplicate($request, $response, $args) {
        $checklistId = $args['id'];
//        $newChecklistName = $args['name'];

        if ($checklistId !== '' && isset($checklistId)) {
            $checklist = Checklist::find($checklistId);

            if ($this->container->auth->user()->id != $checklist->user_id && $checklist->public_view != 1) {
                return 'You shall not pass';
            }

            // Duplicate the checklist
            $new_checklist = $checklist->replicate();
            $new_checklist->name .= ' (copy)';

            // TODO: Add current user to this checklist
            $new_checklist->user_id = $this->container->auth->user()->id;
            $new_checklist->public_view = 0;
            $new_checklist->push();

            $categories = Category::where(array('checklist_id' => $checklistId))->get();

            // Duplicate categories and items
            foreach ($categories as $category) {
                // First reset the new_category
                $new_category = null;
                $new_category = $category->replicate();
                $new_category->checklist_id = $new_checklist->id;
                $new_category->push();

                // First reset the item list
                $items = null;
                $items = Item::where(array('category_id' => $category->id))->get();

                foreach ($items as $item) {
                    // First reset the new_item
                    $new_item = null;
                    $new_item = $item->replicate();
                    $new_item->category_id = $new_category->id;
                    $new_item->push();
                }
            }

            return $response->withRedirect($this->router->pathFor('checklist.add') . $new_checklist->id);
        }

        return $response->withRedirect($this->router->pathFor('checklist.list'));
    }
}
