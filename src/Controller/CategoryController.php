<?php

namespace HolidayChecklist\Controller;

use HolidayChecklist\Model\Category;
use Slim\Container;

class CategoryController {
    protected $container;
    protected $view;

    public function __construct(Container $container) {
        $this->container = $container;
        $this->view = $container['view'];
    }

    /**
     * TODO: Add check if it's a checklist from the user
     * Save Category
     *
     * @param $request
     * @param $response
     * @param $args
     * @return mixed
     */
    public function save($request, $response, $args) {
        $data = $request->getParsedBody();
        $resp = array();

        if (isset($data['id']) && $data['id'] !== '') {
            $cat = Category::find($data['id']);
            $resp['action'] = "saved";
        } elseif (isset($data['name'])) {
            $cat = new Category();
            $resp['action'] = "added";
        } else {
            // Todo: make nice message or something
            return '0';
        }

        $cat->checklist_id = $data['checklist_id'];
        $cat->name = $data['name'];
        $cat->save();
        $resp['category_id'] = $cat->id;

        return $response->withJson($resp);
    }

    /**
     * TODO: Fix removing category when there are items in it
     * TODO: Add check for current user
     * Delete a category
     *
     * @param $request
     * @param $response
     * @param $args
     * @return mixed
     */
    public function delete($request, $response, $args) {
        $data = $request->getParsedBody();
        $resp = array();

        if (isset($data['id']) && $data['id'] !== '') {
            $cat = Category::find($data['id']);

            try {
                $cat->delete();
                $resp['action'] = "removed";
            } catch (\Exception $e) {
                $resp['action'] = "error";
                $resp['messages'][] = "Error when removing category";
            }
        } else {
            $resp['action'] = "notfound";
        }

        return $response->withJson($resp);
    }
}
