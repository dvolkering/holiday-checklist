<?php

namespace HolidayChecklist\Controller;

use HolidayChecklist\Model\Category;
use HolidayChecklist\Model\Checklist;
use HolidayChecklist\Model\Item;
use Slim\Container;
use Slim\Http\Request;
use Slim\Http\Response;

class ItemController {
    protected $container;
    protected $view;

    public function __construct(Container $container) {
        $this->container = $container;
        $this->view = $container['view'];
    }

    /**
     * Saves the title of an item
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response The response with added JSON messages
     */
    public function save($request, $response, $args) {
        if (!$request->isXhr()) {
            return $response;
        }

        $data = $request->getParsedBody();
        $resp = array();
        $resp['action'] = "error";

        // Check if the category is legit
        if (isset($data['category_id']) && $data['category_id'] !== '') {
            $cat = Category::find($data['category_id']);
            $checklist = Checklist::find($cat->checklist_id);

            // Check if the referer contains the checklist id corresponding the category
            if (str_contains($request->getServerParam('HTTP_REFERER'), $cat->checklist_id) === false) {
                $resp['messages'][] = "Checklist ID is not matching";
                return $response->withJson($resp);
            }

            // If the user is changing the DOM it's okay, as long as if it's his own checklist
            if (!AuthController::isUserAllowed($checklist, $this->container->auth->user()->id)) {
                $resp['messages'][] = "User ID is not matching the checklist";
                return $response->withJson($resp);
            }
        } else {
            $resp['messages'][] = "Category ID not found";
            return $response->withJson($resp);
        }

        // Check if it's a new item
        if (isset($data['id']) && $data['id'] !== '') {
            // ID is provided, change the item of the ID
            $item = Item::find($data['id']);
            $resp['action'] = "saved";
        } elseif (isset($data['name'])) {
            // No ID provided, but name is. Create a new item
            $item = new Item();
            $resp['action'] = "added";

            // Only set the category ID when it's a new item
            $item->category_id = $data['category_id'];
        } else {
            // No id and no name provided, this should not be possible so directly give a response.
            $resp['messages'][] = "Invalid post";
            return $response->withJson($resp);
        }

        // Set the new data
        $item->checked = $data['checked'] == '1' ? '1' : '0';
        $item->name = $data['name'];

        // Save or delete the item
        if ($item->name !== '') {
            $item->save();
            $resp['item_id'] = $item->id;
        } else {
            try {
                $item->delete();
                $resp['action'] = "removed";
            } catch (\Exception $e) {
                $resp['messages'][] = "Error removing item";
            }
        }

        return $response->withJson($resp);
    }

    /**
     * TODO: MAKE THIS ROBUST/Handle exception + handle on frontend
     * Save order of items
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response The response with added JSON messages
     */
    public function saveOrder($request, $response, $args) {
        $data = $request->getParsedBody();
        $resp = array();

        foreach ($data['order'] as $order => $itemId) {
            if ($itemId !== '') {
                $item = Item::find($itemId);
                $item->order = $order;
                $item->save();
            }
        }

        return $response->withJson($resp);
    }

    /**
     * Remove Item
     *
     * TODO check for current logged in user
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response The response with added JSON messages
     */
    public function remove($request, $response, $args) {
        $itemId = $args['id'];
        $resp = array();

        if ($itemId !== '' && isset($itemId)) {
            $item = Item::find($itemId);

            try {
                $item->delete();
                $resp['action'] = "removed";
            } catch (\Exception $e) {
                $resp['action'] = "error";
            }

            return $response->withJson($resp);
        } else {
            $resp['action'] = "error";
            $resp['messages'][] = "Item ID not set";
            return $response->withJson($resp);
        }
    }
}
