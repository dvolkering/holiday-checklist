CREATE TABLE IF NOT EXISTS `users` (
  `id`           INT(11)      NOT NULL AUTO_INCREMENT,
  `name`         VARCHAR(255),
  `email`        VARCHAR(255) NOT NULL,
  `password`     VARCHAR(255) NOT NULL,
  `role`         VARCHAR(255),
  `created_at`   DATETIME NOT NULL,
  `updated_at`   DATETIME NOT NULL,
  PRIMARY KEY (`id`)
)
ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `checklists` (
  `id`                INT(11) NOT NULL AUTO_INCREMENT,
  `name`              TEXT     NOT NULL,
  `date_start`        DATETIME,
  `date_end`          DATETIME,
  `public_view`       TINYINT(1) NOT NULL,
  `public_edit`       TINYINT(1) NOT NULL,
  `user_id`           INT(11) NOT NULL,
  `created_at`        DATETIME NOT NULL,
  `updated_at`        DATETIME NOT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (user_id) REFERENCES users(id)
)
ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `users_checklists` (
  `checklist_id`      INT(11) NOT NULL,
  `user_id`           INT(11) NOT NULL,
  PRIMARY KEY (`user_id`, `checklist_id`),
  FOREIGN KEY (checklist_id) REFERENCES checklists(id),
  FOREIGN KEY (user_id) REFERENCES users(id)
)
ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `categories` (
  `id`                INT(11) NOT NULL AUTO_INCREMENT,
  `checklist_id`      INT(11) NOT NULL,
  `name`              TEXT     NOT NULL,
  `created_at`        DATETIME NOT NULL,
  `updated_at`        DATETIME NOT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (checklist_id) REFERENCES checklists(id)
)
ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `items` (
  `id`                INT(11) NOT NULL AUTO_INCREMENT,
  `category_id`       INT(11) NOT NULL,
  `name`              TEXT     NOT NULL,
  `checked`           TINYINT(1) NOT NULL,
  `order`             INT(11),
  `created_at`        DATETIME NOT NULL,
  `updated_at`        DATETIME NOT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (category_id) REFERENCES categories(id)
)
ENGINE=InnoDB;

INSERT INTO `checklists` (`id`, `name`, `date_start`, `date_end`, `created_at`, `updated_at`) VALUES
(1, 'Parijs', '2018-07-03 00:00:00', '2018-07-08 00:00:00', '2018-06-23 17:24:46', '2018-06-23 17:24:46');

INSERT INTO `categories` (`id`, `checklist_id`, `name`, `created_at`, `updated_at`) VALUES
(1, 1, 'Clothes', '2018-06-25 22:08:14', '2018-06-25 22:08:14');

INSERT INTO `items` (category_id, name, checked, created_at, updated_at)  VALUES
(1, 'shirt', 0, '2018-06-23 17:24:46', '2018-06-23 17:24:46'),
(1, 'pants', 0, '2018-06-23 17:24:46', '2018-06-23 17:24:46'),
(1, 'underwear', 0, '2018-06-23 17:24:46', '2018-06-23 17:24:46'),
(1, 'socks', 0, '2018-06-23 17:24:46', '2018-06-23 17:24:46');

-- ALTER TABLE `items` ADD `order` int (11) NULL AFTER `checked`;


-- ALTER TABLE `checklists` ADD `user_id` int (11) NOT NULL AFTER `date_end`;
-- ALTER TABLE `checklists` ADD CONSTRAINT user_id_fk FOREIGN KEY (user_id) REFERENCES users(id);